$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})

$(function(){

  $("#exzoom").exzoom({
    // options here
  });

});

$( document ).ready(function() {
    $("#owl-carousel").owlCarousel({
         items: 3,
         loop: false,
         mouseDrag: false,
         touchDrag: false,
         pullDrag: false,
         rewind: true,
         autoplay: true,
         margin: 0,
         nav: true
     });
});